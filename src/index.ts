import Koa from 'koa';
import KoaRouter from '@koa/router';

const router = new KoaRouter();

router.get('/', ctx => {
	ctx.response.status = 200;
});

const app = new Koa();

app.use(router.routes());
app.use(router.allowedMethods());

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
	console.log(`🚀 Running on port ${PORT}`);
});
